#include<iostream>
using namespace std;
class Matrix
{
private:
	int row;
	int col;
	int **arr;
public:
	Matrix( void )
	{
		this->row = 0;
		this->col = 0;
		this->arr = nullptr;
	}
	Matrix( int row, int col )
	{
		this->row = row;
		this->col = col;
		this->arr = new int*[ this->row ];
		for( int index = 0; index < this->row; ++ index )
			this->arr[ index ] = new int[ this->col ];
	}
	Matrix( const Matrix &other )
	{
		//Step 1 : Copy required size
		this->row = other.row;
		this->col = other.col;
		//Step 2 : Allocate new memory
		this->arr = new int*[ this->row ];
		for( int index = 0; index < this->row; ++ index )
			this->arr[ index ] = new int[ this->col ];
		//Step 3 : Copy the contents
		for( int row = 0; row < this->row; ++ row )
		{
			for( int col = 0; col < this->col; ++ col )
			{
				this->arr[ row ][ col ] = other.arr[ row ][ col ];
			}
		}
	}
	Matrix& operator=( const Matrix &other )
	{
		this->~Matrix( );	//Dealloacte existing memory
		//Step 1 : Copy required size
		this->row = other.row;
		this->col = other.col;
		//Step 2 : Allocate new memory
		this->arr = new int*[ this->row ];
		for( int index = 0; index < this->row; ++ index )
			this->arr[ index ] = new int[ this->col ];
		//Step 3 : Allocate new memory
		for( int row = 0; row < this->row; ++ row )
		{
			for( int col = 0; col < this->col; ++ col )
			{
				this->arr[ row ][ col ] = other.arr[ row ][ col ];
			}
		}
		return *this;
	}
	Matrix operator+( const Matrix &other )const
	{
		Matrix temp( this->row, this->col );
		for( int row = 0; row < this->row; ++ row )
		{
			for( int col = 0; col < this->col; ++ col )
			{
				temp.arr[ row ][ col ] = this->arr[ row ][ col ] + other.arr[ row ][ col ];
			}
		}
		return temp;
	}
	Matrix operator-( const Matrix &other )const
	{
		Matrix temp( this->row, this->col );
		for( int row = 0; row < this->row; ++ row )
		{
			for( int col = 0; col < this->col; ++ col )
			{
				temp.arr[ row ][ col ] = this->arr[ row ][ col ] - other.arr[ row ][ col ];
			}
		}
		return temp;
	}
	Matrix operator*( const Matrix &other )const
	{
		Matrix temp( this->row, this->col );
		for( int row = 0; row < this->row; ++ row )
		{
			for( int col = 0; col < this->col; ++ col )
			{
				temp.arr[ row ][ col ] = 0;
				for( int index = 0; index < this->row; ++ index )
					temp.arr[ row ][ col ] += this->arr[ row ][ index ] * this->arr[ index ][ col ];
			}
		}
		return temp;
	}
	~Matrix( void )
	{
		if( this->arr != nullptr )
		{
			for( int index = 0; index < this->row; ++ index )
				delete[] this->arr[ index ];
			delete[] this->arr;
			this->arr = nullptr;
		}
	}
	friend istream& operator>>( istream &cin, Matrix &other )
	{
		for( int row = 0; row < other.row; ++ row )
		{
			for( int col = 0; col < other.col; ++ col )
			{
				cout<<"Enter arr[ "<<row<<" ][ "<<col<<" ]	:	";
				cin>>other.arr[ row ][ col ];
			}
		}
		return cin;
	}
	friend ostream& operator<<( ostream &cout, const Matrix &other )
	{
		for( int row = 0; row < other.row; ++ row )
		{
			for( int col = 0; col < other.col; ++ col )
			{
				cout<<other.arr[ row ][ col ]<<"	";
			}
			cout<<endl;
		}
		return cout;
	}
};
int menu_list( void )
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Sum"<<endl;
	cout<<"2.Sub"<<endl;
	cout<<"3.Multiplication"<<endl;
	cout<<"4.Get element	:	"<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;
}
int main( void )
{
	int choice;
	Matrix m1(2,3), m2(2,3), m3;
	cin>>m1;
	cin>>m2;
	while( ( choice = ::menu_list( ) ) != 0 )
	{
		switch( choice )
		{
		case 1:
			m3 = m1 + m2;
			cout<<m3<<endl;
			break;
		case 2:
			m3 = m1 - m2;
			cout<<m3<<endl;
			break;
		case 3:
			m3 = m1 * m2;
			cout<<m3<<endl;
			break;
		}
	}
	return 0;
}
