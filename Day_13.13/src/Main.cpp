#include<iostream>
using namespace std;

class Test
{
public:
	void showRecord( void )
	{
		cout<<"void showRecord( void )"<<endl;
	}
	static void displayRecord( void )
	{
		cout<<"void displayRecord( void )"<<endl;
	}
};
int main( void )
{
	Test::displayRecord( );	//OK

	Test t1;
	t1.displayRecord();	//OK
	return 0;
}
int main1( void )
{
	Test t1;
	t1.showRecord( );	//OK
	//Test::showRecord( );	//Not OK
	return 0;
}
