#include<iostream>
using namespace std;
class Matrix
{
private:
	int row;
	int col;
	int **arr;
public:
	Matrix( void )
	{
		this->row = 0;
		this->col = 0;
		this->arr = nullptr;
	}
	Matrix( int row, int col )
	{
		this->row = row;
		this->col = col;
		this->arr = new int*[ this->row ];
		for( int index = 0; index < this->row; ++ index )
			this->arr[ index ] = new int[ this->col ];
	}
	int* operator[]( int index )
	{
		return this->arr[ index ];
	}
	~Matrix( void )
	{
		if( this->arr != nullptr )
		{
			for( int index = 0; index < this->row; ++ index )
				delete[] this->arr[ index ];
			delete[] this->arr;
			this->arr = nullptr;
		}
	}
	friend istream& operator>>( istream &cin, Matrix &other )
	{
		for( int row = 0; row < other.row; ++ row )
		{
			for( int col = 0; col < other.col; ++ col )
			{
				cout<<"Enter arr[ "<<row<<" ][ "<<col<<" ]	:	";
				cin>>other.arr[ row ][ col ];
			}
		}
		return cin;
	}
	friend ostream& operator<<( ostream &cout, const Matrix &other )
	{
		for( int row = 0; row < other.row; ++ row )
		{
			for( int col = 0; col < other.col; ++ col )
			{
				cout<<other.arr[ row ][ col ]<<"	";
			}
			cout<<endl;
		}
		return cout;
	}
};
int main( void )
{
	Matrix m1( 2, 3 );
	cin>>m1;
	cout<<m1<<endl;

	int row, col;
	cout<<"Row	:	";
	cin>>row;
	cout<<"Col	:	";
	cin>>col;
	int element = m1[ row ][ col ];//int element = m1.operator[](row)[col];
	cout<<"m1[ "<<row<<" ][ "<<col<<" ] is : "<<element<<endl;
	return 0;
}
