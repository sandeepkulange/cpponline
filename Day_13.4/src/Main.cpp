#include<iostream>
using namespace std;


void print( void );
//static int main( void )	//'main' is not allowed to be declared static
int main( void )
{
	print( );

	//extern int num1;	//OK
	//cout<<"Num1	:	"<<num1<<endl;	//OK

	//extern int num2;	//OK
	//cout<<"Num2	:	"<<num2<<endl;	//Not OK
	return 0;
}
