#include<iostream>
using namespace std;

class Test
{
private:
	 int num1;			//Instance Variable
	 int num2;			//Instance Variable
	 static int num3;	//Class Level Variable
public:
	Test( void ) : num1( 0 ), num2( 0 )
	{	}
	void setNum1( int num1 )
	{
		this->num1 = num1;
	}
	void setNum2( int num2 )
	{
		this->num2 = num2;
	}
	static void setNum3( int num3 )
	{
		//this->num3 = num3;	//Not OK
		Test::num3 = num3;
	}
	void printRecord( void )const
	{
		cout<<"Num1	:	"<<this->num1<<endl;
		cout<<"Num2	:	"<<this->num2<<endl;
		cout<<"Num3	:	"<<Test::num3<<endl;
		cout<<endl;
	}
};
int Test::num3;	//Global Definition
int main( void )
{
	Test t1;
	t1.setNum1(10);
	t1.setNum2(20);
	Test::setNum3(30);

	t1.printRecord( );
	return 0;
}
