
#ifndef ARRAY_H_
#define ARRAY_H_
#include<iostream>
using namespace std;

#include"../include/IllegalArgumentException.h"

template<class T>
class Array
{
private:
	int size;
	T *arr;
public:
	Array( int size = 0 ) throw( IllegalArgumentException );
	void acecptRecord( void );
	void resize( int size )throw( IllegalArgumentException );
	void printRecord( void )const;
	~Array( void );
};

template<class T>
Array<T>::Array( int size ) throw( IllegalArgumentException )
{
	this->size = size;
	if( this->size == 0 )
		this->arr = nullptr;
	else if( this->size > 0 )
		this->arr = new T[ this->size ];
	else
		throw IllegalArgumentException("Invalid Size");
}
template<class T>
void Array<T>::acecptRecord( void )
{
	for( int index = 0; index < this->size; ++ index )
	{
		cout<<"Enter element	:	";
		cin>>this->arr[ index ];
	}
}
template<class T>
void Array<T>::resize( int size )throw( IllegalArgumentException )
{
	if( size <= 0 )
		throw IllegalArgumentException("Invalid Size");
	if( size > this->size || size < this->size )
	{
		T *arr = new T[ size ];
		for( int index = 0; index < this->size; ++ index )
			arr[ index ] = this->arr[ index ];
		this->size = size;
		this->~Array();
		this->arr = arr;
	}
}
template<class T>
void Array<T>::printRecord( void )const
{
	for( int index = 0; index < this->size; ++ index )
		cout<<this->arr[ index ]<<endl;
}
template<class T>
Array<T>:: ~Array( void )
{
	if( this->arr != nullptr )
	{
		delete[] this->arr;
		this->arr = nullptr;
	}
}

#endif /* ARRAY_H_ */
