#ifndef ADDRESS_H_
#define ADDRESS_H_

#include<iostream>
#include<string>
using namespace std;

class Address
{
private:
	string city;
	string state;
	int pincode;
public:
	Address( void );
	Address( string city, string state, int pincode );
	string getCity( void )const;
	void setCity( string city );
	string getState( void )const;
	void setState( string state );
	int getPincode( void )const;
	void setPincode( int pincode );
	friend istream& operator>>( istream &cin, Address &other );
	friend ostream& operator<<( ostream &cout, const Address &other );
};

#endif /* ADDRESS_H_ */
