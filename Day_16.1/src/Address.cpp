#include"../include/Address.h"
Address::Address( void ) : city(""), state(""), pincode( 0 )
{	}
Address::Address( string city, string state, int pincode ) : city( city ), state( state ), pincode( pincode )
{	}
string Address::getCity( void )const
{
	return this->city;
}
void Address::setCity( string city )
{
	this->city = city;
}
string Address::getState( void )const
{
	return this->state;
}
void Address::setState( string state )
{
	this->state = state;
}
int Address::getPincode( void )const
{
	return this->pincode;
}
void Address::setPincode( int pincode )
{
	this->pincode = pincode;
}
istream& operator>>( istream &cin, Address &other )
{
	cout<<"City	:	";
	cin>>other.city;
	cout<<"State	:	";
	cin>>other.state;
	cout<<"Pincode	:	";
	cin>>other.pincode;
	return cin;
}
ostream& operator<<( ostream &cout, const Address &other )
{
	cout<<other.city<<"	"<<other.state<<"	"<<other.pincode;
	return cout;
}
