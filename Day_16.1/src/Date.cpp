#include"../include/Date.h"
Date::Date( void ) : day( 0 ), month( 0 ), year( 0 )
{	}
Date::Date( int day, int month, int year ) : day( day ), month( month ), year( year )
{	}
int Date::getDay( void )const
{
	return this->day;
}
void Date::setDay( const int day )
{
	this->day = day;
}
int Date::getMonth( void )const
{
	return this->month;
}
void Date::setMonth( const int month )
{
	this->month = month;
}
int Date::getYear( void )const
{
	return this->year;
}
void Date::setYear( const int year )
{
	this->year = year;
}
istream& operator>>( istream &cin, Date &other )
{
	cout<<"Day	:	";
	cin>>other.day;
	cout<<"Month	:	";
	cin>>other.month;
	cout<<"Year	:	";
	cin>>other.year;
	return cin;
}
ostream& operator<<( ostream &cout, const Date &other )
{
	cout<<other.day<<" / "<<other.month<<" / "<<other.year;
	return cout;
}
