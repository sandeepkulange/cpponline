#include"../include/Person.h"
Person::Person( void )
{	}
Person::Person( string name, Date birthDate, Address currentAddress ) : name( name ), birthDate( birthDate ), currentAddress( currentAddress )
{	}
Person::Person( string name, int day, int month, int year, string city, string state, int pincode ) : name( name ), birthDate( day, month, year ), currentAddress( city, state, pincode )
{	}
string Person::getName( void )
{
	return this->name;
}
void Person::setName( string name )
{
	this->name = name;
}
Date& Person::getBirthDate( void )
{
	return this->birthDate;
}
void Person::setBirthDate( Date birthDate )
{
	this->birthDate = birthDate;
}
Address& Person::getCurrentAddress( void )
{
	return this->currentAddress;
}
void Person::setCurrentAddress( Address currentAddress )
{
	this->currentAddress = currentAddress;
}
istream& operator>>( istream &cin, Person &other )
{
	cout<<"Name	:	";
	cin>>other.name;
	cout<<"Birth Date	:	"<<endl;
	cin>>other.birthDate;
	cout<<"Current Address	:	"<<endl;
	cin>>other.currentAddress;
	return cin;
}
ostream& operator<<( ostream &cout, const Person &other )
{
	cout<<"Name	:	"<<other.name<<endl;
	cout<<"Birth Date	:	"<<other.birthDate<<endl;
	cout<<"Current Address	:	"<<other.currentAddress<<endl;
	return cout;
}
