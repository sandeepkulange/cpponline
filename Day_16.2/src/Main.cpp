#include<iostream>
#include<string>
using namespace std;
class Person
{
private:
	string name;	//24 bytes
	int age;		// 4 bytes
public:
	Person( void ) : name(""), age(0)
	{	}
	Person( string name, int age ) : name( name ), age( age )
	{	}
	void showRecord( void )
	{
		cout<<"Name	:	"<<this->name<<endl;
		cout<<"Age	:	"<<this->age<<endl;
	}
};

class Employee
{
private:
	string name;
	int age;
	int empid;
	float salary;
public:
	Employee( void ) : name(""), age(0), empid( 0 ), salary( 0 )
	{	}
	Employee( string name, int age, int empid, float salary ) : name( name ), age( age ), empid( empid ), salary( salary )
	{	}
	void displayRecord( void )
	{
		cout<<"Name	:	"<<this->name<<endl;
		cout<<"Age	:	"<<this->age<<endl;
		cout<<"Empid	:	"<<this->empid<<endl;
		cout<<"Salary	:	"<<this->salary<<endl;
	}
};
int main( void )
{
	//Employee emp;
	Employee emp("Sandeep", 36, 33, 45000.50f );
	emp.displayRecord();
	return 0;
}
int main3( void )
{
	Employee emp;
	size_t size = sizeof( emp );
	cout<<"Size	:	"<<size<<endl;
	return 0;
}
int main2( void )
{
	//Person p;
	Person p("Sandeep",36);
	p.showRecord();
	return 0;
}

int main1( void )
{
	Person p;
	size_t size = sizeof( p );
	cout<<"Size	:	"<<size<<endl;
	return 0;
}
