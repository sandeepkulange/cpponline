#include<iostream>
using namespace std;

#include"../include/Person.h"
using namespace cpponline;

Person::Person( void ) : name(""), age(0)
{	}
Person::Person( string name, int age ) : name( name ), age( age )
{	}
void Person::showRecord( void )
{
	cout<<"Name	:	"<<this->name<<endl;
	cout<<"Age	:	"<<this->age<<endl;
}
