#include<iostream>
#include<string>
using namespace std;

class Person
{
private:
	string name;
	int age;
public:
	Person( void ) : name(""), age(0)
	{	}
	Person( string name, int age ) : name( name ), age( age )
	{	}
	void printRecord( void )
	{
		cout<<"Name	:	"<<this->name<<endl;
		cout<<"Age	:	"<<this->age<<endl;
	}
};
class Employee : public Person
{
private:
	int empid;
	float salary;
public:
	Employee( void ) : empid( 0 ), salary( 0 )
	{	}
	Employee( string name, int age, int empid, float salary ) : Person( name, age ), empid( empid ), salary( salary )
	{	}
	void printRecord( void )
	{
		//this->Person::printRecord();	//OK
		Person::printRecord();	//OK
		cout<<"Empid	:	"<<this->empid<<endl;
		cout<<"Salary	:	"<<this->salary<<endl;
		//printRecord();	//rec Call
	}
};
int main( void )
{
	Employee emp("Sandeep",36,33,45000.50f);
	emp.printRecord();
	//emp.Employee::printRecord();
	return 0;
}
int main1( void )
{
	Employee emp("Sandeep",36,33,45000.50f);
	emp.Person::printRecord();
	emp.Employee::printRecord();
	return 0;
}
