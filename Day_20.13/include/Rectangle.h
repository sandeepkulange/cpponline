#ifndef RECTANGLE_H_
#define RECTANGLE_H_

#include"../include/Shape.h"
#include"../include/IllegalArgumentException.h"
using namespace cpponline;

namespace cpponline
{
	class Rectangle : public Shape
	{
	private:
		float length;
		float breadth;
	public:
		Rectangle( void )throw( );
		void setLength( const float length )throw( IllegalArgumentException );
		void setbreadth( const float breadth )throw( IllegalArgumentException );
		void calculateArea( void )throw( );
	};
}

#endif /* RECTANGLE_H_ */
