#ifndef SHAPEFACTORY_H_
#define SHAPEFACTORY_H_

#include"../include/Shape.h"
using namespace cpponline;

namespace cpponline
{
	class ShapeFactory
	{
	public:
		static Shape* getObject( int choice );
	};
}

#endif /* SHAPEFACTORY_H_ */
