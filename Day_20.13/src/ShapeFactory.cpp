#include "../include/Rectangle.h"
#include "../include/Circle.h"
using namespace cpponline;
#include"../include/ShapeFactory.h"
Shape* ShapeFactory::getObject( int choice )
{
	Shape *ptr = nullptr;
	switch( choice )
	{
	case 1:
		ptr = new Rectangle();	//Upcasting
		break;
	case 2:
		ptr = new Circle( );	//Upcasting
		break;
	}
	return ptr;
}
