#include<iostream>
using namespace std;
class Base
{
private:
	int num1;
	int num2;
public:
	Base( int num1 = 0, int num2 = 0 ) : num1( num1 ), num2( num2 )
	{	}
	void print( void )
	{
		cout<<"Num1	:	"<<this->num1<<endl;
		cout<<"Num2	:	"<<this->num2<<endl;
	}
};
class Derived : public Base
{
private:
	int num3;
public:
	Derived( int num1 = 0, int num2 = 0, int num3 = 0 ) : Base(num1, num2), num3( num3 )
	{	}
	void print( void )
	{
		Base::print();
		cout<<"Num3	:	"<<this->num3<<endl;
	}
};
int main( void )
{
	Base *ptr = new Derived();
	cout<<typeid( ptr ).name()<<endl;	//P4Base
	cout<<typeid( *ptr ).name()<<endl;	//4Base
	delete ptr;
	return 0;
}
int main4( void )
{
	Derived *ptr = new Derived();
	//cout<<typeid( ptr ).name()<<endl;	//P7Derived
	cout<<typeid( *ptr ).name()<<endl;	//7Derived
	delete ptr;
	return 0;
}
int main3( void )
{
	Derived obj;
	cout<<typeid( obj ).name()<<endl;	//7Derived
	return 0;
}
int main2( void )
{
	Base *ptr = new Base();
	//cout<<typeid( ptr ).name()<<endl;	//P4Base
	cout<<typeid( *ptr ).name()<<endl;	//4Base
	delete ptr;
	return 0;
}
int main1( void )
{
	Base obj;
	cout<<typeid( obj ).name()<<endl;	//4Base
	return 0;
}
