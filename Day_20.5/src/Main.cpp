#include<iostream>
using namespace std;
class Test
{
private:
	int number;
public:
	//Test *const this = &t
	Test( void ) : number( 10 )
	{	}
	//Test *const this = &t
	void show( void )
	{
		cout<<"Number	:	"<<this->number<<endl;
	}
	//const Test *const this = &t
	void print( void )const
	{
		//Test *const ptr = (Test *const)this;
		Test *const ptr = const_cast<Test *const>( this );
		ptr->show();
	}
};
int main( void )
{
	const Test t;
	t.print();	//t.print( &t ) ;
	return 0;
}
