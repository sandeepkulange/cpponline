#include<iostream>
#include<string>
using namespace std;

namespace nemployee
{
	class Employee
	{
	private:
		string name;
		int empid;
		float salary;
	public:
		void acceptRecord( void )
		{
			cout<<"Name	:	";
			cin>>name;
			cout<<"Empid	:	";
			cin>>empid;
			cout<<"Salary	:	";
			cin>>salary;
		}
		void printRecord( void )
		{
			cout<<"Name	:	"<<name<<endl;
			cout<<"Empid	:	"<<empid<<endl;
			cout<<"Salary	:	"<<salary<<endl;
		}
	};
}
int main( void )
{
	using namespace nemployee;
	Employee emp;


	cout<<sizeof(emp)<<endl;
	cout<<&emp<<endl;
	return 0;
}
int main3( void )
{
	using namespace nemployee;
	Employee emp1,emp2, emp3;

	emp1.acceptRecord();
	emp2.acceptRecord();
	emp3.acceptRecord();

	emp1.printRecord();
	emp2.printRecord();
	emp3.printRecord();
	return 0;
}
int main2( void )
{
	using namespace nemployee;
	Employee emp;

	size_t size = sizeof( emp );
	cout<<"Size	:	"<<size<<endl;
	return 0;
}

int main1( void )
{
	using namespace nemployee;
	Employee emp;
	emp.acceptRecord();
	emp.printRecord();
	return 0;
}
