#include<iostream>
using namespace std;

//void sum( int num1, int num2 )
//void sum( int num1, int num2, int num3  )	//OK


//void sum( int num1, int num2 )
//void sum( int num1, double num2 )	//OK

//void sum( int num1, float num2 )
//void sum( float num1, int num2 )	//OK


//int sum( int num1, int num2 )
//void sum( int num1, int num2 )	//Not OK

int sum( int num1, int num2 )
{
	int result = num1 + num2;
	cout<<"Result	:	"<<result<<endl;
	return result;
}
void sum( int num1, int num2 )
{
	int result = num1 + num2;
	cout<<"Result	:	"<<result<<endl;
}
int main( void )
{
	int result = sum( 10, 20 );

	sum( 50, 60 );
	return 0;
}
