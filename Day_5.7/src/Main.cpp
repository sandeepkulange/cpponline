#include<iostream>
using namespace std;
void print( int number )
{
	cout<<"int	:	"<<number<<endl;
}
void print( int *number )
{
	cout<<"int*	:	"<<number<<endl;
}
int main( void )
{
	int number = 10;
	//print( number );	//int	:	10
	//print( &number );	//int*	:	0x7ffeed276308
	//print( NULL );	//Ambiguity Error
	//print(nullptr );	//int*	:	0x0

	return 0;
}
