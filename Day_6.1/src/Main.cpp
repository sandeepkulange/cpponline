#include<iostream>
using namespace std;
int main( void )
{
	const int number = 10;	// OK
	cout<<"Number	:	"<<number<<endl;
	return 0;
}
int main3( void )
{
	//const int number;	//Not OK
	//cout<<"Number	:	"<<number<<endl;
	return 0;
}
int main2( void )
{
	const int number = 10;	//OK
	//number = number + 5;	//Not OK
	cout<<"Number	:	"<<number<<endl;
	return 0;
}
int main1( void )
{
	int number = 10;
	number = number + 5;	//OK
	cout<<"Number	:	"<<number<<endl;
	return 0;
}
