#include<iostream>
using namespace std;
class Test
{
private:
	const int num1;
public:
	Test( void ) : num1( 10 )	//OK
	{
		//this->num1 = 10;	//Not OK
		//++ this->num1;	//Not OK
	}
	//Test *const this = &t1;
	void showRecord( void )
	{
		//++ this->num1;	//Not OK
		cout<<"Num1	:	"<<this->num1<<endl;	//10
	}
	void printRecord( void )
	{
		//++ this->num1;	//Not OK
		cout<<"Num1	:	"<<this->num1<<endl;	//20
	}
};
int main( void )
{
	Test t1;
	t1.showRecord( );
	t1.printRecord( );
	return 0;
}

