#include<iostream>
#include<string>
using namespace std;
class ArithmeticException
{
private:
	string message;
public:
	ArithmeticException( string message ) : message( message )
	{ }
	string getMessage( void )
	{
		return this->message;
	}
};
void accept_record( int &number )
{
	cout<<"Enter number	:	";
	cin>>number;
}
int calculate(int num1, int num2 )
{
	if( num2 == 0 )
		throw ArithmeticException("/ by 0");
	return num1 / num2;
}
void print_record( int &result )
{
	cout<<"Result	:	"<<result<<endl;
}
int main( void )
{
	int num1;
	accept_record( num1 );

	int num2;
	accept_record( num2 );
	try
	{
		int result = calculate( num1, num2 );
		print_record( result );
	}
	catch( ArithmeticException &ex )
	{
		cout<<ex.getMessage()<<endl;
	}
	catch(...)
	{
		cout<<"Inside generic catch"<<endl;
	}
	return 0;
}
