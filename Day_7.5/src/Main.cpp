#include<iostream>
#include<string>
using namespace std;

class IllegalArgumentException
{
private:
	string message;
public:
	IllegalArgumentException( string message = "IllegalArgumentException" ) throw( );

	string getMessage( void )const throw( );
};

IllegalArgumentException::IllegalArgumentException( string message ) throw( ) : message( message )
{	}
string IllegalArgumentException::getMessage( void )const throw( )
{
	return this->message;
}


class Complex
{
private:
	int real;
	int imag;
public:
	Complex( int real = 0, int imag = 0 )throw( IllegalArgumentException);

	//const Complex *const this = &complex;
	int getReal( void )const throw( );

	//Complex *const this = &complex;
	void setReal( const int real )throw ( IllegalArgumentException );

	//const Complex *const this = &complex;
	int getImag( void )const throw( );

	//Complex *const this = &complex;
	void setImag( const int imag )throw ( IllegalArgumentException );
};

Complex::Complex( int real, int imag )throw( IllegalArgumentException)
{
	this->setReal(real);
	this->setImag(imag);
}
//const Complex *const this = &complex;
int Complex::getReal( void )const throw( )
{
	return this->real;
}
//Complex *const this = &complex;
void Complex::setReal( const int real )throw ( IllegalArgumentException )
{
	if( real >= 0 )
		this->real = real;
	else
		throw IllegalArgumentException("Invalid real number");
}
//const Complex *const this = &complex;
int Complex::getImag( void )const throw( )
{
	return this->imag;
}
//Complex *const this = &complex;
void Complex::setImag( const int imag )throw ( IllegalArgumentException )
{
	if( imag >= 0 )
		this->imag = imag;
	else
		throw IllegalArgumentException("Invalid imag number");
}
void accept_record( Complex &complex )throw( IllegalArgumentException )
{
	int real;
	cout<<"Real Number	:	";
	cin>>real;
	complex.setReal( real );

	int imag;
	cout<<"Imag Number	:	";
	cin>>imag;
	complex.setImag( imag );
}
void print_record( const Complex &complex )throw( )
{
	int real = complex.getReal( );
	cout<<"Real Number	:	"<<real<<endl;

	int imag = complex.getImag();
	cout<<"Imag Number	:	"<<imag<<endl;
}
int menu_list( void )
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Accept Record"<<endl;
	cout<<"2.Print Record"<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;
}
int main( void )
{
	int choice;
	Complex complex;
	while( ( choice = ::menu_list( ) ) != 0 )
	{
		try
		{
			switch( choice )
			{
			case 1:
				::accept_record(complex);
				break;
			case 2:
				::print_record(complex);
				break;
			}
		}
		catch (IllegalArgumentException &ex)
		{
			cout<<ex.getMessage()<<endl;
		}
	}
	return 0;
}
