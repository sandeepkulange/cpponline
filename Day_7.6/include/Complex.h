#ifndef COMPLEX_H_
#define COMPLEX_H_

#include"../include/IllegalArgumentException.h"
using namespace cpponline;

namespace cpponline
{
	class Complex
	{
	private:
		int real;
		int imag;
	public:
		Complex( int real = 0, int imag = 0 )throw( IllegalArgumentException);

		//const Complex *const this = &complex;
		int getReal( void )const throw( );

		//Complex *const this = &complex;
		void setReal( const int real )throw ( IllegalArgumentException );

		//const Complex *const this = &complex;
		int getImag( void )const throw( );

		//Complex *const this = &complex;
		void setImag( const int imag )throw ( IllegalArgumentException );
	};
}
#endif /* COMPLEX_H_ */
