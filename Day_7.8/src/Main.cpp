#include<iostream>
#include<string>
using namespace std;
int main( void )
{
	int number = 10;
	void *ptr1 = &number;
	//cout<<*ptr1<<endl;	//Not OK

	int *ptr2 = (int*)ptr1;
	cout<<*ptr2<<endl;	// OK
	return 0;
}
int main2( void )
{
	void *ptr = nullptr;

	char a = 'A';
	ptr = &a;
	cout<<a<<"	"<<endl;	//Ok

	//cout<<*ptr<<"	"<<endl;//Not OK

	int b = 10;
	ptr = &b;
	cout<<b<<"	"<<endl;//Ok
	//cout<<*ptr<<"	"<<endl;//Not OK

	double c = 20.2;
	ptr = &c;
	cout<<c<<"	"<<endl;//Ok
	//cout<<*ptr<<"	"<<endl;//Not OK

	string d = "SunBeam";
	ptr = &d;
	cout<<d<<"	"<<endl;//Ok
	//cout<<*ptr<<"	"<<endl;//Not OK
	return 0;
}
int main1( void )
{
	char a = 'A';
	char *pa = &a;
	cout<<a<<"	"<<*pa<<endl;

	int b = 10;
	int *pb= &b;
	cout<<b<<"	"<<*pb<<endl;

	double c = 20.2;
	double *pc = &c;
	cout<<c<<"	"<<*pc<<endl;

	string d = "SunBeam";
	string *pd = &d;
	cout<<d<<"	"<<*pd<<endl;
	return 0;
}
