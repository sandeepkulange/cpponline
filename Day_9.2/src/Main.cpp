#include<iostream>
using namespace std;
class Complex
{
private:
	int real;
	int imag;
public:
	Complex( void )
	{
		cout<<"Complex( void )"<<endl;
		this->real = 0;
		this->imag = 0;
	}
	//Complex *const this = &c2;
	//Complex &other = c1
	Complex( const Complex &other )
	{
		cout<<"Complex( const Complex &other )"<<endl;
		this->real = other.real;
		this->imag = other.imag;
	}
	Complex( int real, int imag )
	{
		cout<<"Complex( int real, int imag )"<<endl;
		this->real = real;
		this->imag = imag;
	}
	Complex sum( Complex other )
	{
		Complex temp;
		temp.real = this->real + other.real;
		temp.imag = this->imag + other.imag;
		return temp;
	}
	void printRecord( void )
	{
		cout<<"Real Number	:	"<<this->real<<endl;
		cout<<"Imag Number	:	"<<this->imag<<endl;
	}
};
int main( void )
{
	Complex c1( 10, 20 );
	Complex c2( 30, 40 );
	Complex c3;
	c3 = c1.sum(c2);
	c3.printRecord();
	return 0;
}
int main5( void )
{
	Complex c1(10,20);	//Complex( int real, int imag )
	Complex c2( c1 );	//Complex( const Complex &other )
	return 0;
}
int main4( void )
{
	Complex c1(10,20);	//Complex( int real, int imag )
	Complex c2 = c1;	//Complex( const Complex &other )
	return 0;
}
int main3( void )
{
	Complex c1(10,20);	//Complex( int real, int imag )
	Complex c2;	//Complex( void )
	c2 = c1;	//c2.operator=( c1 )
	return 0;
}
int main2( void )
{
	Complex c1(10,20);	//Complex( int real, int imag )
	Complex &c2 = c1;	//No ctor will call
	return 0;
}
int main1( void )
{
	Complex c1(10,20);	//Complex( int real, int imag )
	Complex *c2 = &c1;	//No ctor will call
	return 0;
}
